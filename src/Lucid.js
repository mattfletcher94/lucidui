import LucidComponent from "./LucidComponent";
var Lucid = /** @class */ (function () {
    function Lucid() {
    }
    Lucid.Component = LucidComponent;
    return Lucid;
}());
export default Lucid;
