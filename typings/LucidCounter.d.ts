import ILucidComponent from "./ILucidComponent";
interface Counter extends ILucidComponent {
    state: {
        count: number;
        title: string;
    };
    methods: {
        handleClick: (e: MouseEvent) => void;
        handleDblClick: (e: MouseEvent) => void;
    };
}
declare const _default: import("./LucidComponent").default<Counter>;
export default _default;
