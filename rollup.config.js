import pkg from './package.json';
import resolve from 'rollup-plugin-node-resolve';
import commonJS from 'rollup-plugin-commonjs';
import {terser} from "rollup-plugin-terser";


export default {
    input: 'src/index.js',
    output: [
        {
            file: pkg.module,
            format: 'es',
        },
        {
            name: 'Lucid',
            file: pkg.main,
            format: 'iife'
        },
        {
            name: 'Lucid',
            file: pkg.browser,
            format: 'iife',
            plugins: [
                terser()
            ]
        },
    ],
    external: [
        ...Object.keys(pkg.devDependencies || {}),
        ...Object.keys(pkg.peerDependencies || {}),
    ],
    plugins: [
        resolve(),
        commonJS(),
    ],
    onwarn: function(warning) {
        if ( warning.code === 'THIS_IS_UNDEFINED' ) { return; }
        console.warn( warning.message );
    }
}