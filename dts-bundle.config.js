var pkg = require('./package.json');
var dts = require('dts-bundle');
dts.bundle({
    name: pkg.name,
    main: 'typings/index.d.ts',
    out: '../' + pkg.types,
    referenceExternals: true
});