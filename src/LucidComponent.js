var LucidComponent = /** @class */ (function () {
    function LucidComponent(config) {
        this.__registerState = config.state;
        this.__registerWatchers = config.watchers;
        this.__registerMethods = config.methods;
        this.__registerTemplate = config.template;
        this.__registerConnected = config.connected;
        this.__registerDisconnected = config.disconnected;
    }
    LucidComponent.prototype.create = function ($el) {
        var _this = this;
        // Get required data from the el we're about to replace
        var oldSlot = $el.innerHTML;
        var oldClassNames = $el.className;
        var oldID = $el.id;
        var oldStyle = $el.getAttribute('style');
        // Start building the lucid component
        var component = {};
        var watchers = this.__registerWatchers(component);
        component.__publicWatchers = [];
        component.$refs = {};
        component.$slot = oldSlot;
        component.state = new Proxy(this.__registerState(component), {
            set: function (state, key, value) {
                var oldvalue = state[key];
                var watcher = watchers[key];
                state[key] = value;
                if (watcher) {
                    watcher.apply(_this, [value, oldvalue]);
                }
                component.__publicWatchers.forEach(function (w) {
                    if (w.key === key) {
                        w.callback.apply(component, [value, oldvalue]);
                    }
                });
                return true;
            },
            get: function (state, key) {
                return state[key];
            },
        });
        component.methods = this.__registerMethods(component);
        component.watch = function (key, callback) {
            component.__publicWatchers.push({
                key: key,
                callback: callback
            });
        };
        component.unwatch = function (key) {
            var i = component.__publicWatchers.length;
            while (i--) {
                if (component.__publicWatchers[i].key === key) {
                    component.__publicWatchers.splice(i, 1);
                }
            }
        };
        // Create the template
        var template = this.__parseTemplate(this.__registerTemplate(component));
        // Create the new HTML element and replace the old one with it.
        var $fragment = document.createRange().createContextualFragment(template).childNodes;
        if ($fragment.length != 1) {
            throw new Error('Lucid Error: Component template should contain one root element.');
        }
        var $newEl = $fragment[0];
        var combinedStyles = "";
        if (oldStyle) {
            combinedStyles = oldStyle;
        }
        if ($newEl.getAttribute('style')) {
            combinedStyles = combinedStyles + ';' + $newEl.getAttribute('style');
        }
        if (combinedStyles) {
            $newEl.setAttribute('style', $newEl.getAttribute('style') + ";" + oldStyle);
        }
        var combinedClasses = "";
        if (oldClassNames) {
            combinedClasses = oldClassNames;
        }
        if ($newEl.className) {
            combinedClasses = combinedClasses + ' ' + $newEl.className;
        }
        if (combinedClasses) {
            $newEl.setAttribute('class', combinedClasses);
        }
        var combinedID = "";
        if (oldID) {
            combinedID = oldID;
        }
        else if ($newEl.id) {
            combinedID = $newEl.id;
        }
        if (combinedID) {
            $newEl.setAttribute('id', combinedID);
        }
        $el.replaceWith($newEl);
        component.$el = $newEl;
        // Parse refs and events
        var elements = Array.from($newEl.querySelectorAll("*"));
        elements.push($newEl);
        elements.forEach(function (element) {
            var el = element;
            var attributes = el.attributes;
            var i = attributes.length;
            while (i--) {
                var attribute = attributes[i];
                if (attribute.nodeName === 'data-lucid-ref') {
                    component.$refs[attribute.nodeValue] = el;
                    el.removeAttribute(attribute.nodeName);
                }
                else if (attribute.nodeName.includes('data-lucid-event-')) {
                    var event_1 = attribute.nodeName.split('-').pop();
                    var method = attribute.nodeValue;
                    if (typeof component.methods[method] !== 'function') {
                        throw new Error('Lucid Error: Method ' + method + ' does not exist on ' + name);
                    }
                    el.addEventListener(event_1, component.methods[method].bind(component));
                    el.removeAttribute(attribute.nodeName);
                }
            }
        });
        component.destroy = function () {
            var _a;
            (_a = component.$el.parentNode) === null || _a === void 0 ? void 0 : _a.replaceChild(component.$el.cloneNode(true), component.$el);
            if (_this.__registerDisconnected) {
                _this.__registerDisconnected(component);
            }
        };
        if (this.__registerConnected) {
            this.__registerConnected(component);
        }
        // Return component
        return component;
    };
    LucidComponent.prototype.__parseTemplate = function (template) {
        return template.replace(/@on.([^\s]+)="([^\s]+)"/g, function (match, event, method) {
            return "data-lucid-event-" + event + "=\"" + method + "\"";
        }).replace(/@ref="([^\s]+)"/g, function (match, refName) {
            return "data-lucid-ref=\"" + refName + "\"";
        });
    };
    return LucidComponent;
}());
export default LucidComponent;
