export default interface ILucidComponent {
    state: {
        [key: string]: any;
    };
    methods: {
        [key: string]: Function;
    };
}
