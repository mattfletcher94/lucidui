import ILucidComponent from "./ILucidComponent";
export default interface LucidComponentReturn<T extends ILucidComponent> {
    $el: HTMLElement;
    $refs: {
        [key: string]: HTMLElement;
    };
    $slot: string;
    state: T['state'];
    methods: T['methods'];
    destroy: () => void;
    watch: <K extends keyof T['state']>(name: K, callback: (val: T['state'][K], old: T['state'][K]) => void) => void;
    unwatch: <K extends keyof T['state']>(name: K) => void;
    __publicWatchers: Array<{
        key: keyof T['state'];
        callback: Function;
    }>;
}
