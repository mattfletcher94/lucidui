import ILucidComponent from "./ILucidComponent";
import ILucidComponentReturn from "./ILucidComponentReturn";
export default interface ILucidComponentConfig<T extends ILucidComponent> {
    state: (component: ILucidComponentReturn<T>) => T['state'];
    methods: (component: ILucidComponentReturn<T>) => T['methods'];
    watchers: (component: ILucidComponentReturn<T>) => {
        [K in keyof T['state']]: (val: T['state'][K], oldVal: T['state'][K]) => void;
    };
    template: (component: ILucidComponentReturn<T>) => string;
    connected?: (component: ILucidComponentReturn<T>) => void;
    disconnected?: (component: ILucidComponentReturn<T>) => void;
}
