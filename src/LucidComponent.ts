import ILucidComponentConfig from "./ILucidComponentConfig";
import ILucidComponentReturn from "./ILucidComponentReturn";
import ILucidComponent from "./ILucidComponent";


export default class LucidComponent<T extends ILucidComponent> {

    public __registerState : (component : ILucidComponentReturn<T>) => T['state'];
    public __registerWatchers : (component : ILucidComponentReturn<T>) => { [K in keyof T['state']] : (val : T['state'][K], oldVal : T['state'][K]) => void; };
    public __registerMethods : (component : ILucidComponentReturn<T>) => T['methods'];
    public __registerTemplate : (component : ILucidComponentReturn<T>) => string;
    public __registerConnected : ((component : ILucidComponentReturn<T>) => void) | undefined;
    public __registerDisconnected : ((component : ILucidComponentReturn<T>) => void) | undefined;

    public constructor (config : ILucidComponentConfig<T>) {
        this.__registerState = config.state;
        this.__registerWatchers = config.watchers;
        this.__registerMethods = config.methods;
        this.__registerTemplate = config.template;
        this.__registerConnected = config.connected;
        this.__registerDisconnected = config.disconnected;
    }

    public create($el : HTMLElement) : ILucidComponentReturn<T> {

        // Get required data from the el we're about to replace
        var oldSlot = $el.innerHTML;
        var oldClassNames = $el.className;
        var oldID = $el.id;
        var oldStyle = $el.getAttribute('style');

        // Start building the lucid component
        var component = {} as ILucidComponentReturn<T>;
        var watchers = this.__registerWatchers(component);
        component.__publicWatchers = [];
        component.$refs = {};
        component.$slot = oldSlot;
        component.state = new Proxy(this.__registerState(component), {
            set: (state : any, key : any, value : any) => {
                const oldvalue = state[key];
                const watcher = watchers[key];
                state[key] = value;
                if (watcher) {
                    watcher.apply(this, [value, oldvalue]);
                }
                component.__publicWatchers.forEach((w) => {
                    if (w.key === key) {
                        w.callback.apply(component, [value, oldvalue]);
                    }
                });
                return true;
            },
            get: (state : any, key : any) => {
                return state[key];
            },
        });
        component.methods = this.__registerMethods(component);
        component.watch = (key, callback) => {
            component.__publicWatchers.push({
                key: key,
                callback : callback
            });
        };
        component.unwatch = (key) => {
            var i = component.__publicWatchers.length;
            while (i--) {
                if (component.__publicWatchers[i].key === key) { 
                    component.__publicWatchers.splice(i, 1);
                } 
            }
        };

        // Create the template
        const template = this.__parseTemplate(this.__registerTemplate(component));

        // Create the new HTML element and replace the old one with it.
        var $fragment = document.createRange().createContextualFragment(template).childNodes;
        if ($fragment.length != 1) {
            throw new Error('Lucid Error: Component template should contain one root element.');
        }
        var $newEl = $fragment[0] as HTMLElement;

        var combinedStyles = "";
        if (oldStyle) {
            combinedStyles = oldStyle;
        }
        if ($newEl.getAttribute('style')) {
            combinedStyles = combinedStyles + ';' + $newEl.getAttribute('style');
        }
        if (combinedStyles) {
            $newEl.setAttribute('style', `${$newEl.getAttribute('style')};${oldStyle}`);
        }

        var combinedClasses = "";
        if (oldClassNames) {
            combinedClasses = oldClassNames;
        }
        if ($newEl.className) {
            combinedClasses = combinedClasses + ' ' + $newEl.className;
        }
        if (combinedClasses) {
            $newEl.setAttribute('class', combinedClasses);
        }

        var combinedID = "";
        if (oldID) {
            combinedID = oldID;
        } else if ($newEl.id) {
            combinedID = $newEl.id;
        }
        if (combinedID) {
            $newEl.setAttribute('id', combinedID);
        }

        $el.replaceWith($newEl);
        component.$el = $newEl;


        // Parse refs and events
        var elements = Array.from($newEl.querySelectorAll("*"));
        elements.push($newEl);
        elements.forEach((element : Element) => {
            const el = element as HTMLElement;
            const attributes = el.attributes;
            let i = attributes.length;
            while(i--) {
                const attribute = attributes[i];
                if (attribute.nodeName === 'data-lucid-ref') {
                    component.$refs[attribute.nodeValue as string] = el;
                    el.removeAttribute(attribute.nodeName);
                }
                else if (attribute.nodeName.includes('data-lucid-event-')) {
                    let event = attribute.nodeName.split('-').pop() as string;
                    let method = attribute.nodeValue as string;
                    if (typeof component.methods[method] !== 'function') {
                        throw new Error('Lucid Error: Method ' + method + ' does not exist on ' + name);
                    }
                    el.addEventListener(event, component.methods[method].bind(component));
                    el.removeAttribute(attribute.nodeName);
                }
            }
        });

        component.destroy = () => {
            component.$el.parentNode?.replaceChild(component.$el.cloneNode(true), component.$el);
            if (this.__registerDisconnected) {
                this.__registerDisconnected(component);
            }
        }

        if (this.__registerConnected) {
            this.__registerConnected(component);
        }

        // Return component
        return component;
    }

    public __parseTemplate(template : string) : string {
        return template.replace(/@on.([^\s]+)="([^\s]+)"/g, function(match, event, method) {
            return `data-lucid-event-${event}="${method}"`;
        }).replace(/@ref="([^\s]+)"/g, function(match, refName) {
            return `data-lucid-ref="${refName}"`;
        });
    }

}