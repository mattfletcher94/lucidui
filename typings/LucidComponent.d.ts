import ILucidComponentConfig from "./ILucidComponentConfig";
import ILucidComponentReturn from "./ILucidComponentReturn";
import ILucidComponent from "./ILucidComponent";
export default class LucidComponent<T extends ILucidComponent> {
    __registerState: (component: ILucidComponentReturn<T>) => T['state'];
    __registerWatchers: (component: ILucidComponentReturn<T>) => {
        [K in keyof T['state']]: (val: T['state'][K], oldVal: T['state'][K]) => void;
    };
    __registerMethods: (component: ILucidComponentReturn<T>) => T['methods'];
    __registerTemplate: (component: ILucidComponentReturn<T>) => string;
    __registerConnected: ((component: ILucidComponentReturn<T>) => void) | undefined;
    __registerDisconnected: ((component: ILucidComponentReturn<T>) => void) | undefined;
    constructor(config: ILucidComponentConfig<T>);
    create($el: HTMLElement): ILucidComponentReturn<T>;
    __parseTemplate(template: string): string;
}
